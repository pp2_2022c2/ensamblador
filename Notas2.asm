%include "io.inc"

section .data
    vec1 dw 5,2,3,4
    vec2 dw 1,0,0,0
    msg db "Valor: %d",10,13
section .text
global CMAIN
CMAIN:
    
    mov ebp, esp; for correct debugging
    mov ebx, 4

    mov ecx, 1
    movq mm0, qword[vec1]
    movq mm1, qword[vec2]
    movd mm2, ecx
    pmullw mm1,mm0
    ;psrlq mm0, 16  
    movd eax, mm1


    jmp recorrer
    
recorrer:
    sub ebx, 1
    
    movq mm3, mm0
    pmullw mm3, mm2
    movd eax, mm3
    
    push eax
    push msg
    call printf 
    add esp, 8
    
    psrlq mm0, 16
    
    cmp ebx, 0
    jnz recorrer
    
    ret

