%include "io.inc"

section .data
    vec1 dw 5,2,3,4
    vec2 dw 1,0,0,0
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging

    movq mm0, qword[vec1]
    movq mm1, qword[vec2]
    pmullw mm1,mm0
    psrlq mm0, 16  
    movd eax, mm1

    
    

    xor eax, eax
    ret