%include "io.inc"

section .data
    vec1 db 1,2,3,4,5
    vec2 db 6,7,8,9,0
    dim db 5
    cont db 0
    
section .bss
    vec3 resb 5

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    
    mov eax, dim
    push eax
    mov edx,vec1
    call cargarVectores       ;recibe vector en eax, dimension en ebx
    mov edx, vec2
    call apilarVector       ;recibe vector en edx y dimencion en pila
               
    call sumar              
    add esp,4
    
    xor eax, eax
    ret
    
cargarVectores:
    
    
apilarVector:
    push ebp
    mov ebp, esp
    
    mov eax,[ebp+4]         ;traigo dim
    
    cmp eax, 4    
    jg apilar4
    cmp eax,2
    jg apilar3
    je apilar2
    cmp eax,0
    jz terminar
    jmp apilar1
     
    
apilar4:                    ;recibe dimension en eax
    
    mov ecx, [edx]
    push ecx                ;pushe los priemros 4 byte=32 bit

    sub eax, 4
    push eax
    
    add cont,1
    
    jmp apilarVector
    
apilar1:                        ;recibe dimension en eax
    ;calculamos el offset
    mul eax, 4
    
    ;sumamos el offset y pusheamos
    xor ecx,ecx
    mov cl,[edx+eax]   
    push ecx

    push 0
    jmp apilarVector

retorno:
    sub cont,1
    mov esp,ebp
    pop ebp
    
    cmp cont
    jnz retorno
    ret

terminar:
    

sumarSimd: 
    movq mm0, qword[esp+4]
    movq mm1, qword[esp+12]
    
    paddsb mm0,mm1
    
    ret

mover:
    ;movd vec3, mm0    
    
    ret
