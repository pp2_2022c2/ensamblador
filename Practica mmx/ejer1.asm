%include "io.inc"

section .data
    vec1 db 2,4,6,8,10,12,14,16
    vec2 db 1,2,3,4,5,6,7,8
    vec3 db 3,1,0,7,1,2,3,5
    msg db "Esto: %hhx",10,13

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    
    push eax
    mov eax, [vec1]
    push eax
    
    mov eax, [vec2+4]
    push eax
    mov eax, [vec2]
    push eax
    
    mov eax, [vec3+4]
    push eax
    mov eax, [vec3]
    push eax
    
    call cargar
    call restarVectores
    call print
    add esp,24

    xor eax,eax
    ret
   
cargar:
    push ebp
    mov ebp, esp
    
    movq mm0, qword[ebp+24]
    
    mov esp, ebp
    pop ebp
    
    ret
    
restarVectores:
    push ebp
    mov ebp,esp
    
    movq mm0, qword[ebp+24]
    movq mm1, qword[ebp+16]
    
    psubsb mm0,mm1              ;guarda en mm0
    
    movq mm1, qword[ebp+8]
    
    psubsb mm0, mm1
    
    mov esp,ebp
    pop ebp
    
    ret

print:    
    push ebp
    mov ebp,esp
    
    xor ecx,ecx
    mov ecx,3987654321
    movd mm5,ecx
    movd eax,mm5
    
    movd ebx,mm0

    push ebx
    push msg
    call printf
    add esp,4
    
    mov esp,ebp
    pop ebp    
    
    ret
    