%include "io.inc"

section .data
    vec1 db 1,2,3,4,5
    vec2 db 6,7,8,9,0
    dim db 5
    
section .bss
    vec3 resb 5

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    
    xor eax,eax
    mov al, [vec1+4]
    push eax
    mov eax, vec1
    push eax
    
    xor eax, eax
    mov al, [vec2+4]
    push eax
    mov eax, vec2
    push eax
    
    call sumarSimd
    call mover
    add esp, 16
    

    xor eax, eax
    ret

sumarSimd: 
    movq mm0, qword[esp+4]
    movq mm1, qword[esp+12]
    
    paddsb mm0,mm1
    
    ret

mover:
    movd vec3, mm0    
    
    ret
