global aclarar
global multiplyBlend
global medianFilter
section .text
;1)----------------------------------------------

aclarar:
        push    ebp
        mov     ebp, esp

        mov     DWORD  [ebp-8], 0               ;i=0
for1:
        cmp     DWORD  [ebp-8], 511             ;i<511
        jg      salir
        mov     DWORD  [ebp-12], 0              ;j=0
for2:        
        cmp     DWORD  [ebp-12], 511            ;j<511
        jg      volver

        ;red -----------------------------------
        mov     eax, DWORD  [ebp-8]             ;i
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+8]             ;red
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            ;j
        add     eax, ecx

        movq    mm0,qword[eax]
        movq    mm1,[ebp+20]                    ;n
        paddb     mm0,mm1                        

        movq    [eax],mm0
        ;movq    [eax+1],mm0

        ;green -----------------------------------
        mov     eax, DWORD  [ebp-8]             ;i
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+12]            ;green
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            ;j
        add     eax, ecx

        movq    mm0,qword[eax]
        movq    mm1,[ebp+20]                    ;n
        paddb     mm0,mm1                       

        movq    [eax],mm0
        ;movq    [eax+1],mm0                     
       
        ;blue -----------------------------------
        mov     eax, DWORD  [ebp-8]             ;i
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+16]            ;blue
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            ;j
        add     eax, ecx

        movq    mm0,qword[eax]
        movq    mm1,[ebp+20]                    ;n
        paddb     mm0,mm1                       

        
        movq    [eax],mm0
        ;movq    [eax+1],mm0                     
         

        add     DWORD  [ebp-12],1
        jmp     for2
volver:
        inc     DWORD  [ebp-8]
        jmp     for1
salir:
        nop
        pop     ebp
        ret

;2)----------------------------------------------

promedio:
        push    ebp
        mov     ebp, esp
        sub     esp, 16                         ;reservamos

        mov     DWORD  [ebp-4], 0               ;suma
        mov     DWORD  [ebp-8], 0               ;cantidad
        mov     eax,    [ebp+8]                 ;indice1 int
        mov     DWORD  [ebp-12], eax            ;guardo indice1
for12:
        mov     edx, DWORD  [ebp+8]             ;indice1
        mov     eax, DWORD  [ebp+20]            ;window
        add     eax, edx                        ;indice2+window
        cmp     DWORD  [ebp-12], eax            ;j <= j2+windows
        jg     salir21

        mov     eax, DWORD  [ebp+12]            ;puntero1 a arr1
        mov     DWORD  [ebp-16], eax            ;cargo en la pila puntero a arr2

        movd    mm0,    [eax]
        
for22:
        mov     edx, DWORD  [ebp+12]            ;indice2
        mov     eax, DWORD  [ebp+20]            ;window
        add     eax, edx                        ;P**arr1 + window
        cmp     DWORD  [ebp-16], eax            ;j > indice2+window
        jg     volver31


        mov     eax, DWORD  [ebp-12]            ;indice1
        lea     edx, [0+eax*4]
        mov     eax, DWORD  [ebp+16]            ;window
        add     eax, edx                        ;suma el offset
        mov     edx, DWORD  [eax]               ;
        mov     eax, DWORD  [ebp-16]
        add     eax, edx
        movq    mm0, [eax]
        mov     al, BYTE  [eax]
        and     eax, 255
        add     DWORD  [ebp-4], eax

        inc     DWORD  [ebp-8]
        inc     DWORD  [ebp-16]
        jmp     for22

volver31:
        inc     DWORD  [ebp-12]
        jmp     for12

salir21:
        mov     eax, DWORD  [ebp-4]
        mov     edx, eax
        sar     edx, 31
        idiv    DWORD  [ebp-8]
        leave
        ret

medianFilter:
        push    ebp
        mov     ebp, esp
        sub     esp, 16
        mov     ebx, DWORD [ebp+20]
        mov     DWORD  [ebp-4], ebx               ;i
        
for122:
        mov     eax, 512                        
        sub     eax, ebx                        ;i< 512-windows
        cmp     DWORD  [ebp-4], eax             
        jge     salir2
        mov     DWORD  [ebp-8], ebx             ;j
for222:
        mov     eax, 512
        sub     eax, DWORD  [ebp+20]
        cmp     DWORD  [ebp-8], eax             ;j< 512-windows
        jge     volver3

        ;red ------------------------
        push    DWORD [ebp+20]
        push    DWORD  [ebp+8]                  ;puntero matriz red
        push    DWORD  [ebp-8]                  ;j = window o mas
        push    DWORD  [ebp-4]                  ;i = window o mas
        call    promedio

        add     esp, 16
        mov     edx, eax
        mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+8]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl

        ;green ------------------------
        push    DWORD [ebp+20]
        push    DWORD  [ebp+12]
        push    DWORD  [ebp-8]
        push    DWORD  [ebp-4]
        call    promedio

        add     esp, 16
        mov     edx, eax
        mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+12]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl

        ;blue ------------------------
        push    DWORD [ebp+20]
        push    DWORD  [ebp+16]
        push    DWORD  [ebp-8]
        push    DWORD  [ebp-4]
        call    promedio

        add     esp, 16
        mov     edx, eax
        mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+16]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl

        inc     DWORD  [ebp-8]
        jmp     for222

volver3:
        inc     DWORD  [ebp-4]
        jmp     for122
salir2:
        nop
        leave
        ret

;3)----------------------------------------------

multiplyBlend:
        push    ebp
        mov     ebp, esp
        mov     DWORD  [ebp-8], 0               ;i
for11:
        cmp     DWORD  [ebp-8], 511             ;i<512
        jg      salir
        mov     DWORD  [ebp-12], 0              ;j
for21:
        cmp     DWORD  [ebp-12], 511            ;j<512
        jg      volver2

        ;green ----------------------------------
        mov     eax, DWORD  [ebp-8]
        lea     edx, [0+eax*4]
        mov     eax, DWORD  [ebp+12]
        add     eax, edx
        mov     edx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, edx
        movq    mm0,    [eax]
        mov     al, BYTE  [eax]

        mov     edx, DWORD  [ebp-8]
        lea     ecx, [0+edx*4]
        mov     edx, DWORD  [ebp+24]
        add     edx, ecx
        mov     ecx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ecx
        mov     cl, BYTE  [edx]

        mov     edx, DWORD  [ebp-8]
        lea     ebx, [0+edx*4]
        mov     edx, DWORD  [ebp+12]
        add     edx, ebx
        mov     ebx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ebx
        imul    eax, ecx                        ;green1[i][j] *=green2[i][j];
        mov     BYTE  [edx], al
        ;------------------------------

        ;blue ------------------------
        mov     eax, DWORD  [ebp-8]
        lea     edx, [0+eax*4]
        mov     eax, DWORD  [ebp+16]
        add     eax, edx
        mov     edx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, edx
        mov     al, BYTE  [eax]

        mov     edx, DWORD  [ebp-8]
        lea     ecx, [0+edx*4]
        mov     edx, DWORD  [ebp+28]
        add     edx, ecx
        mov     ecx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ecx
        mov     cl, BYTE  [edx]

        mov     edx, DWORD  [ebp-8]
        lea     ebx, [0+edx*4]
        mov     edx, DWORD  [ebp+16]
        add     edx, ebx
        mov     ebx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ebx
        imul    eax, ecx                        ;blue1[i][j] *=blue2[i][j];
        mov     BYTE  [edx], al
        ;------------------------------

        ;red -------------------------
        mov     eax, DWORD  [ebp-8]
        lea     edx, [0+eax*4]
        mov     eax, DWORD  [ebp+8]
        add     eax, edx
        mov     edx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, edx
        mov     al, BYTE  [eax]

        mov     edx, DWORD  [ebp-8]
        lea     ecx, [0+edx*4]
        mov     edx, DWORD  [ebp+20]
        add     edx, ecx
        mov     ecx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ecx
        mov     cl, BYTE  [edx]

        mov     edx, DWORD  [ebp-8]
        lea     ebx, [0+edx*4]
        mov     edx, DWORD  [ebp+8]
        add     edx, ebx
        mov     ebx, DWORD  [edx]
        mov     edx, DWORD  [ebp-12]
        add     edx, ebx
        imul    eax, ecx                        ;red1[i][j] *=red2[i][j];
        mov     BYTE  [edx], al
        ;------------------------------

        inc     DWORD  [ebp-12]
        jmp     for21
volver2:
        inc     DWORD  [ebp-8]
        jmp     for11
















